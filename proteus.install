<?php

/**
 * @file
 * Install and update functions
 */


/**
 * Implements hook_schema().
 */
function proteus_schema() {
  $schema = array();
  $schema['proteus_user_objective_level'] = array(
    'description' => 'The level that a user has gained for an objective',
    'fields' => array(
      'uid' => array(
        'description' => 'The user id',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'termid' => array(
        'description' => 'The objective (taxonomy term)',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'curlevel' => array(
        'description' => 'The current level of this user for this objective',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'maxlevel' => array(
        'description' => 'The highest level the user ever reached for this objective',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('uid', 'termid'),
  );

  $schema['proteus_quiz_objective_target'] = array(
    'description' => 'The target level of an objective for a quiz',
    'fields' => array(
      'nid' => array(
        'description' => 'The quiz node id',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'termid' => array(
        'description' => 'The objective (taxonomy term)',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'targetlevel' => array(
        'description' => 'The target level for this objective for this quiz',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('nid', 'termid'),
    'indexes' => array(
      'nid' => array('nid'),
      'termid' => array('termid'),
    ),
  );

  $schema['proteus_question_objective'] = array(
    'description' => 'The objective enter and exit level for a question',
    'fields' => array(
      'nid' => array(
        'description' => 'The question node id',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'termid' => array(
        'description' => 'The objective (taxonomy term)',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'enterlevel' => array(
        'description' => 'The minimum level for this objective needed to get this question',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'exitlevel' => array(
        'description' => 'The level for this objective when the question is answered correctly in 1 try',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('nid', 'termid'),
  );


  $schema['proteus_user_objective_log'] = array(
    'description' => 'The log table that tracks user progression',
    'fields' => array(
      'log_id' => array(
        'description' => 'The id of the log-entry.',
        'type' => 'serial',
        'not null' => TRUE,
      ),
      'time' => array(
        'description' => 'Timestamp of the log entry',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'uid' => array(
        'description' => 'The user id',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'tid' => array(
        'description' => 'The vocabulary term id belonging to the question',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'quizid' => array(
        'description' => 'The quiz id linked to this log entry',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'questionid' => array(
        'description' => 'The question id linked to this log entry',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'newlevel' => array(
        'description' => 'If a question was answered, the new level for the user for the quiz',
        'type' => 'int',
        'not null' => TRUE,
      ),
      'levelinc' => array(
        'description' => 'If a question was answered, the increase in level for the user for the quiz',
        'type' => 'int',
        'not null' => TRUE,
      ),
      'tries' => array(
        'description' => 'If a question was answered, the number of tries the user needed to answer the question',
        'type' => 'int',
        'unsigned' => TRUE,
        'size' => 'small',
        'not null' => TRUE,
      ),
      'msg' => array(
        'description' => 'An explanatory message regarding the log entry',
        'type' => 'varchar',
        'length' => '50',
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('log_id'),
    'indexes' => array(
      'uidqid' => array('uid', 'quizid'),
    ),
  );

  $schema['proteus_quiz'] = array(
    'description' => 'Extra data for a ProteusQuiz Node.',
    'fields' => array(
      'nid' => array(
        'description' => 'The NodeId of the ProteusQuiz',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'vid' => array(
        'description' => 'The version id.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'exit_text' => array(
        'description' => 'The text that is displayed when the user finished the ProteusQuiz',
        'type' => 'text',
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('nid', 'vid'),
    'indexes' => array(
      'proteus_quiz_nid' => array('nid'),
    ),
  );

  return $schema;
}

/**
 * Implements hook_install().
 */
function proteus_install() {
  // Set the weight of the proteus.module to 1 so it is loaded after the taxonomy.module.
  // Taken as an example from the forum.module
  db_update('system')
    ->fields(array('weight' => 1))
    ->condition('name', 'proteus')
    ->execute();
}

/**
 * Implements hook_uninstall().
 */
function proteus_uninstall() {
  // Taken as an example from the forum.module

  // Load the dependent Taxonomy module, in case it has been disabled.
  drupal_load('module', 'taxonomy');

  // For now, do not remove because the vocabulary isn't removed
  //variable_del('proteus_quizes_vocabulary');

  // Remove all other variables
  variable_del('proteus_use_default_step');
  variable_del('proteus_show_progress');
  
  field_delete_field('taxonomy_proteus_quizes');
  // Purge field data now to allow taxonomy module to be uninstalled
  // if this is the only field remaining.
  field_purge_batch(10);
}


/**
 * Implements hook_enable().
 */
function proteus_enable() {
  // Taken as an example from the forum.module

  // If we enable proteus at the same time as taxonomy we need to call
  // field_associate_fields() as otherwise the field won't be enabled until
  // hook modules_enabled is called which takes place after hook_enable events.
  field_associate_fields('taxonomy');
  
  // Create the forum vocabulary if it does not exist, i.e. previously installed and later de-installed
  $vocabulary = taxonomy_vocabulary_load(variable_get('proteus_quizes_vocabulary', 0));
  if (!$vocabulary) {
    $edit = array(
      'name' => t('Proteus quizes'),
      'machine_name' => 'proteus_quizes',
      'description' => t('Proteus quizes vocabulary'),
      'hierarchy' => 1,
      'module' => 'proteus',
      'weight' => -10,
    );
    $vocabulary = (object) $edit;
    taxonomy_vocabulary_save($vocabulary);
    variable_set('proteus_quizes_vocabulary', $vocabulary->vid);
  }

  // Create the 'taxonomy_proteus' field if it doesn't already exist.
  if (!field_info_field('taxonomy_proteus_quizes')) {
    $field = array(
      'field_name' => 'taxonomy_proteus_quizes',
      'type' => 'taxonomy_term_reference',
      'settings' => array(
        'allowed_values' => array(
          array(
            'vocabulary' => $vocabulary->machine_name,
            'parent' => 0,
          ),
        ),
      ),
    );
    field_create_field($field);

    // Create an initial proteus quiz term as an example.
    $termquery = taxonomy_get_term_by_name( t('Example Proteus quiz'), $vocabulary->machine_name );
    if ( empty( $termquery ) )
    {
      $edit = array(
        'name' => t('Example Proteus quiz'),
        'description' => t('This taxonomy term is created as an example to be used by the Proteus module'),
        'parent' => array(0),
        'vid' => $vocabulary->vid,
      );
      $term = (object) $edit;
      taxonomy_term_save($term);
    }

    // Create the instance on the bundle.
    $instance = array(
      'field_name' => 'taxonomy_proteus_quizes',
      'entity_type' => 'node',
      'label' => $vocabulary->name,
      'bundle' => 'proteus',
      'required' => FALSE,      // Be sure to set this to false. It isn't needed directly
      'widget' => array(
        'type' => 'options_select',
      ),
      'display' => array(
        'default' => array(
          'type' => 'taxonomy_term_reference_link',
          'weight' => 10,
        ),
      ),
    );
    field_create_instance($instance);
  }

  // Ensure the proteus node type is available.
  node_types_rebuild();
  $types = node_type_get_types();
  // Add the body field to this node type
  node_add_body_field($types['proteus'], t('Instruction text displayed on the question-selection screen.'));
}


/**
 * Update to Drupal 7
 */
function proteus_update_7000() {
  $schema = proteus_schema();
  
  // Add a primary key field to the 'proteus_user_objective_log' table
  if (db_table_exists('proteus_user_objective_log')) {
    // Copy the the existing table to a temporary table
    db_rename_table('proteus_user_objective_log', 'd6_proteus_user_objective_log');
  }
  db_create_table('proteus_user_objective_log', $schema['proteus_user_objective_log']);
  $query = db_select('d6_proteus_user_objective_log', 'log')->fields('log');
  foreach ($query->execute() as $row) {
    db_insert('proteus_user_objective_log')->fields(array(
      'time' => $row->time,
      'uid' => $row->uid,
      'tid' => $row->tid,
      'quizid' => $row->quizid,
      'questionid' => $row->questionid,
      'newlevel' => $row->newlevel,
      'levelinc' => $row->levelinc,
      'tries' => $row->tries,
      //'evalId' => $row->evalId,       // This field is dropped in the Drupal 7 version of Proteus
      'msg' => $row->msg,
    ))->execute();
  }
  db_drop_table( 'd6_proteus_user_objective_log' );
}
